<?php

namespace Wagento\Customer\Observer;

use Magento\Customer\Model\Customer;

class CreateHash implements \Magento\Framework\Event\ObserverInterface {

    protected $_logger;

    public function __construct(\Psr\Log\LoggerInterface $logger){
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $event = $observer->getEvent();
        $customer = $event->getCustomer();
        try {
            $customer->setData("hash", $this->_getHash($customer))->save();
        } catch (\Exception $e){
            $this->_logger->debug($e->getMessage());
        }
        
        return $this;
    }

    private function _getHash(Customer $customer){
        return md5(base64_encode($customer->getPassword() . $customer->getId() . $customer->getEmail()));
    }
}