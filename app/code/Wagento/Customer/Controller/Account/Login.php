<?php

namespace Wagento\Customer\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Login extends \Magento\Customer\Controller\Account\Index {

    protected $_customer;
    protected $_session;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Customer\Collection $customer,
        \Magento\Customer\Model\Session $session,
        Context $context,
        PageFactory $resultPageFactory)
    {
        $this->_customer = $customer;
        $this->_session = $session;
        parent::__construct($context, $resultPageFactory);
    }

    public function execute()
    {
        $hash = $this->getRequest()->getParam("hash");
        $customer = $this->_getCustomerByHash($hash);
        if ($customer != NULL){
            $this->_session->setCustomerAsLoggedIn($customer);
            $this->_session->setCustomer($customer);
            $this->_redirect("customer/account/index");
        } else {
            $this->_redirect("/");
        }
    }

    private function _getCustomerByHash($hash){
        $customer = $this->_customer->addFieldToFilter("hash", $hash);
        if ($customer->getSize()){
            return $customer->getFirstItem();
        }

        return NULL;
    }
}
